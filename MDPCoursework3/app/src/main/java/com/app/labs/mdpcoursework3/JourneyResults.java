package com.app.labs.mdpcoursework3;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.app.labs.mdpcoursework3.ContentProvider.DBHelper;
import com.app.labs.mdpcoursework3.ContentProvider.JourneyProviderContract;

public class JourneyResults extends AppCompatActivity
{

    Button okButton = null;
    TextView timeStarted = null;
    TextView totalJourneyTime = null;
    TextView distanceTravelled = null;
    TextView moneySaved = null;
    TextView co2Saved = null;
    TextView timeLost = null;
    TextView lostOrGained = null;

    long journeyId = 0;

    private final String JOURNEY_ID = "journey_id";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journey_results);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        journeyId = getIntent().getExtras().getLong(JOURNEY_ID);

        okButton = findViewById(R.id.okButton);
        okButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                setResult(Activity.RESULT_OK, new Intent());
                finish();
            }
        });

        timeStarted = findViewById(R.id.time_started_value);
        totalJourneyTime = findViewById(R.id.total_travel_time_value);
        distanceTravelled = findViewById(R.id.distance_travelled_value);
        timeLost = findViewById(R.id.time_lost_gain_value);
        moneySaved = findViewById(R.id.money_saved_value);
        co2Saved = findViewById(R.id.co2_saved_value);
        lostOrGained = findViewById(R.id.mins_lost_lost);

        populateResultsWithDbData();

    }

    private void populateResultsWithDbData(){
        Cursor journeyCursor = getJourneyFromDbWithMatchingId(journeyId);
        //if recipe re  turned, set the fields based on its column values
        if(journeyCursor.moveToFirst()){
            long timeStartedValue = journeyCursor.getLong(journeyCursor.getColumnIndexOrThrow(JourneyProviderContract.START_TIME));
            long totalJourneyTimeValue = journeyCursor.getLong(journeyCursor.getColumnIndexOrThrow(JourneyProviderContract.TOTAL_JOURNEY_TIME));
            float distanceTravelledValue = journeyCursor.getFloat(journeyCursor.getColumnIndexOrThrow(JourneyProviderContract.DISTANCE_TRAVELLED));
            long timeLostValue = journeyCursor.getLong(journeyCursor.getColumnIndexOrThrow(JourneyProviderContract.MINS_LOST));
            float moneySavedValue = journeyCursor.getFloat(journeyCursor.getColumnIndexOrThrow(JourneyProviderContract.MONEY_SAVED));
            float co2SavedValue = journeyCursor.getFloat(journeyCursor.getColumnIndexOrThrow(JourneyProviderContract.CO2_SAVED));

            String timeStartedFormatted = DBHelper.formatTimeStarted(timeStartedValue, false);
            String journeyTimeFormatted = DBHelper.formatJourneyTime(totalJourneyTimeValue);
            String distanceTravelledFormatted = DBHelper.formatDistanceTravelled(distanceTravelledValue);

            if(timeLostValue < 0){
                timeLostValue = Math.abs(timeLostValue);
                lostOrGained.setText("gained");
            }

            timeStarted.setText(timeStartedFormatted);
            totalJourneyTime.setText(journeyTimeFormatted);
            distanceTravelled.setText(distanceTravelledFormatted + " miles");
            //TEMP
            timeLost.setText(Long.toString(timeLostValue));
            moneySaved.setText("£" + Float.toString(moneySavedValue));
            co2Saved.setText(Float.toString(co2SavedValue)+"kg");

        }
    }

    private Cursor getJourneyFromDbWithMatchingId(long journeyId){
        return getContentResolver().query(
                JourneyProviderContract.JOURNEYS_URL,  // The content URI of the words table
                new String[]{JourneyProviderContract.START_TIME, JourneyProviderContract.TOTAL_JOURNEY_TIME, JourneyProviderContract.DISTANCE_TRAVELLED,
                        JourneyProviderContract.MINS_LOST, JourneyProviderContract.CO2_SAVED, JourneyProviderContract.MONEY_SAVED}, // The columns to return for each row
                "_id=" + journeyId,                  // Either null, or the word the user entered
                null,                    // Either empty, or the string the user entered
                null);                       // The sort order for the returned rows
    }
}
