package com.app.labs.mdpcoursework3.ContentProvider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DBHelper extends SQLiteOpenHelper
{

    public DBHelper(Context context)
    {
        super(context, "journeyDB", null, 1);
    }

    public DBHelper(Context context, String dbName, SQLiteDatabase.CursorFactory factory, int version)
    {
        super(context, dbName, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE journeys (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "title VARCHAR(128), " +
                "moneySaved FLOAT(24)," +
                "co2Saved FLOAT(24)," +
                "minsLost FLOAT(24)," +
                "startTime BIGINT," +
                "endTime BIGINT," +
                "totalJourneyTime BIGINT," +
                "distanceTravelled FLOAT(24)," +
                "startLat FLOAT(24)," +
                "startLong FLOAT(24)," +
                "endLat FLOAT(24)," +
                "endLong FLOAT(24)" +
                ");");

        db.execSQL("CREATE TABLE journeyRouteNodes (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "journey_id INTEGER REFERENCES journeys(_id) ON UPDATE CASCADE, " +
                "longitude FLOAT(24)," +
                "latitude FLOAT(24)," +
                "altitude FLOAT(24)," +
                "time BIGINT" +
                ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS journeys");
        db.execSQL("DROP TABLE IF EXISTS journeyRouteNodes");
        onCreate(db);
    }

    public static String formatTimeStarted(long timeStartedInMillis, boolean showDate){
        Date currentDate = new Date(timeStartedInMillis);
        DateFormat df = null;
        if(showDate){
            df = new SimpleDateFormat("dd MMM     hh:mm a");
        } else {
            //change format to display just time
            df = new SimpleDateFormat("hh:mm a");
        }
        return df.format(currentDate);
    }
    public static String formatJourneyTime(long journeyTimeInMillis){
        //ns to seconds
        String formatted = String.format("%dm %ds",
                TimeUnit.MILLISECONDS.toMinutes(journeyTimeInMillis),
                TimeUnit.MILLISECONDS.toSeconds(journeyTimeInMillis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(journeyTimeInMillis)));
        return formatted;
    }

    public static String formatDistanceTravelled(float distanceInMeters){
        return String.format("%.3f", distanceInMeters /1609.344f);
    }
}
