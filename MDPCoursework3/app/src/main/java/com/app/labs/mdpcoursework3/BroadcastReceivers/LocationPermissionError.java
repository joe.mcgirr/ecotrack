package com.app.labs.mdpcoursework3.BroadcastReceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.app.labs.mdpcoursework3.RequestPermissions;


public class LocationPermissionError extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        Log.w("MDPCW3", "BCR onReceive");
        Intent activityIntent = new Intent(context, RequestPermissions.class);
        context.startActivity(activityIntent);
    }
}
