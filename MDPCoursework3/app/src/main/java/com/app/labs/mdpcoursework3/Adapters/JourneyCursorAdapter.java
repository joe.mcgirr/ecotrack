package com.app.labs.mdpcoursework3.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.app.labs.mdpcoursework3.ContentProvider.DBHelper;
import com.app.labs.mdpcoursework3.ContentProvider.JourneyProviderContract;
import com.app.labs.mdpcoursework3.R;

public class JourneyCursorAdapter extends CursorAdapter
{
    boolean showDate = false;
    public JourneyCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {
        return LayoutInflater.from(context).inflate(R.layout.journey_list_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {
        TextView journeyTitle = (TextView) view.findViewById(R.id.journey_title_val);
        TextView timeStarted = (TextView) view.findViewById(R.id.time_started_val);
        TextView durationAndDistance = (TextView) view.findViewById(R.id.duration_and_distance_val);
        TextView moneySaved = (TextView) view.findViewById(R.id.money_saved_val);
        TextView co2Saved = (TextView) view.findViewById(R.id.co2_saved_value);

        long timeStartedValue = cursor.getLong(cursor.getColumnIndexOrThrow(JourneyProviderContract.START_TIME));
        long durationValue = cursor.getLong(cursor.getColumnIndexOrThrow(JourneyProviderContract.TOTAL_JOURNEY_TIME));
        float distanceTravelledValue = cursor.getFloat(cursor.getColumnIndexOrThrow(JourneyProviderContract.DISTANCE_TRAVELLED));

        String timeStartedFormatted = null;
        if (showDate){
            timeStartedFormatted = DBHelper.formatTimeStarted(timeStartedValue, true);
        } else {
            timeStartedFormatted = DBHelper.formatTimeStarted(timeStartedValue, false);
        }

        String journeyTimeFormatted = journeyTimeFormatted = DBHelper.formatJourneyTime(durationValue);
        String distanceTravelledFormatted = DBHelper.formatDistanceTravelled(distanceTravelledValue);

        journeyTitle.setText(cursor.getString(cursor.getColumnIndexOrThrow(JourneyProviderContract.TITLE)));
        moneySaved.setText("£" + cursor.getString(cursor.getColumnIndexOrThrow(JourneyProviderContract.MONEY_SAVED)));
        co2Saved.setText(cursor.getString(cursor.getColumnIndexOrThrow(JourneyProviderContract.CO2_SAVED)) + "kg");
        timeStarted.setText(timeStartedFormatted);
        durationAndDistance.setText(journeyTimeFormatted + "   •   " + distanceTravelledFormatted +"mi");
    }

    public void showDateOnListView(boolean showDate){
        this.showDate = showDate;
    }
}
