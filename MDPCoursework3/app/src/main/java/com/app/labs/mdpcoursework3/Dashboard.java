package com.app.labs.mdpcoursework3;

import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toolbar;

import com.app.labs.mdpcoursework3.Adapters.JourneyCursorAdapter;
import com.app.labs.mdpcoursework3.ContentProvider.JourneyProviderContract;

import java.util.Calendar;

public class Dashboard extends AppCompatActivity
{
    static final int ADD_NEW_JOURNEY_REQUEST_CODE = 1;
    static final String DATE_FILTER = "date_filter";

    TextView moneySaved = null;
    TextView co2Saved = null;
    TextView minLost = null;
    ListView journeyList = null;
    TabLayout tabLayout = null;
    JourneyCursorAdapter dataAdapter = null;

    //stores the currently selected date tab
    int currentDateFilter = Calendar.DATE;

    String[] journeyColumns = new String[] {
            JourneyProviderContract._ID,
            JourneyProviderContract.START_TIME,
            JourneyProviderContract.TITLE,
            JourneyProviderContract.TOTAL_JOURNEY_TIME,
            JourneyProviderContract.DISTANCE_TRAVELLED,
            JourneyProviderContract.CO2_SAVED,
            JourneyProviderContract.MONEY_SAVED
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }


    //on add button in toolbar clicked
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add_journey) {
            Intent intent = new Intent(getBaseContext(), RecordJourney.class);
            startActivityForResult(intent, ADD_NEW_JOURNEY_REQUEST_CODE);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume(){
        super.onResume();
        /*Resize listview to fit children and populate money/co2/time saved/lost*/
        setListViewHeightBasedOnChildren(journeyList);
        populateEcoValues(currentDateFilter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        if(savedInstanceState != null){
            currentDateFilter = savedInstanceState.getInt(DATE_FILTER);
        }

        //select tab that corresponds to selected date filter var
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        setSelectedTab(tabLayout);

        //Eco-savings values
        minLost = findViewById(R.id.time_lost_gain_value);
        co2Saved = findViewById(R.id.co2_saved_value);
        moneySaved = findViewById(R.id.money_saved_value);

        //create the cursor adapter and set it for the listview
        setAdapterForCurrentDateSelection(tabLayout.getSelectedTabPosition(), true);
        journeyList = (ListView) findViewById(R.id.journey_list);
        journeyList.setFocusable(false);
        journeyList.setAdapter(dataAdapter);

        /*Add contentObserver to be notified of changes to the db and update list accordingly*/
        Handler h = new Handler();
        getContentResolver().registerContentObserver(
                JourneyProviderContract.JOURNEYS_URL, true, new JourneyChangeObserver(h));

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState)
    {
        savedInstanceState.putInt(DATE_FILTER, currentDateFilter);
        super.onSaveInstanceState(savedInstanceState);
    }

    private void setSelectedTab(TabLayout tabLayout){
        //YEAR = 1, MONTH = 2 minus 1 to get the correct indices of tabs
        int index = currentDateFilter - 1;
        TabLayout.Tab tabs = null;
        //DATE = 5
        if (index > 1){
            tabs = tabLayout.getTabAt(2);
        }
        else {
            tabs = tabLayout.getTabAt(index);
        }
        tabs.select();
        addTabListener(tabLayout);
    }

    private void populateEcoValues(int dateField){
        //datefield = day,month or year
        long endOfCurrentDayInMillis = getEndOfCurrentDayInMillis();
        long startOfCurrentDateFieldInMillis = getStartOfCurrentTimePeriodInMillis(dateField);
        String selection = JourneyProviderContract.START_TIME + " >= " + startOfCurrentDateFieldInMillis + " AND " +
                JourneyProviderContract.START_TIME + " < " + endOfCurrentDayInMillis;
        populateMoneySaved(selection);
        populateCo2Saved(selection);
        populateTimeLost(selection);
    }
    private void populateMoneySaved(String selection){
        String[] columns = new String[] { "sum(" + JourneyProviderContract.MONEY_SAVED + ")" };
        Cursor cursor = getContentResolver().query(JourneyProviderContract.JOURNEYS_URL,
                columns , selection, null, null);
        cursor.moveToFirst();
        float columnSum = cursor.getFloat(0);
        moneySaved.setText(String.format("£%.2f", columnSum));
    }
    private void populateCo2Saved(String selection){
        String[] columns = new String[] { "sum(" + JourneyProviderContract.CO2_SAVED + ")" };
        Cursor cursor = getContentResolver().query(JourneyProviderContract.JOURNEYS_URL,
                columns , selection, null, null);
        cursor.moveToFirst();
        float columnSum = cursor.getFloat(0);
        co2Saved.setText(String.format("%.2fkg", columnSum));
    }
    private void populateTimeLost(String selection){
        String[] columns = new String[] { "sum(" + JourneyProviderContract.MINS_LOST + ")" };
        Cursor cursor = getContentResolver().query(JourneyProviderContract.JOURNEYS_URL,
                columns , selection, null, null);
        cursor.moveToFirst();
        float columnSum = cursor.getFloat(0);
        TextView lostOrGained = findViewById(R.id.mins_lost_lost);
        if(columnSum < 0){
            columnSum = Math.abs(columnSum);
            lostOrGained.setText("gained");
        } else {
            lostOrGained.setText("lost");
        }
        minLost.setText(String.format("%d", Math.round(columnSum)));
    }

    private void initJourneyList(String selection){
        String sortingOrder = JourneyProviderContract.START_TIME + " DESC";

        final Cursor cursor = getContentResolver().query(JourneyProviderContract.JOURNEYS_URL,
                journeyColumns, selection, null, sortingOrder);
        dataAdapter = new JourneyCursorAdapter(this, cursor);
        dataAdapter.notifyDataSetChanged();
    }

    //initialise signifies if the dataadapter needs to be initialised or updated
    private void setAdapterForCurrentDateSelection(int dateTabSelection, boolean initialise){
        long endOfCurrentDayInMillis = getEndOfCurrentDayInMillis();
        switch (dateTabSelection){
            case 0:
                long startOfCurrentYearInMillis = getStartOfCurrentTimePeriodInMillis(Calendar.YEAR);
                String yearlySelection = JourneyProviderContract.START_TIME + " >= " + startOfCurrentYearInMillis + " AND " +
                        JourneyProviderContract.START_TIME + " < " + endOfCurrentDayInMillis;
                if (initialise){
                    initJourneyList(yearlySelection);
                    //setlistviewheigh done in onresume so will be performed after this
                } else {
                    updateAdapterWithNewQuerySelection(yearlySelection);
                    setListViewHeightBasedOnChildren(journeyList);
                }
                populateEcoValues(Calendar.YEAR);
                dataAdapter.showDateOnListView(true);
                currentDateFilter = Calendar.YEAR;
                break;
            case 1:
                long startOfCurrentMonthInMillis = getStartOfCurrentTimePeriodInMillis(Calendar.MONTH);
                String monthlySelection = JourneyProviderContract.START_TIME + " >= " + startOfCurrentMonthInMillis + " AND " +
                        JourneyProviderContract.START_TIME + " < " + endOfCurrentDayInMillis;

                if (initialise){
                    initJourneyList(monthlySelection);
                    //setlistviewheight done in onresume so will be performed after this
                } else {
                    updateAdapterWithNewQuerySelection(monthlySelection);
                    setListViewHeightBasedOnChildren(journeyList);
                }
                populateEcoValues(Calendar.MONTH);
                dataAdapter.showDateOnListView(true);
                currentDateFilter = Calendar.MONTH;
                break;
            case 2:
                long startOfCurrentDayInMillis = getStartOfCurrentTimePeriodInMillis(Calendar.DATE);
                String dailySelection = JourneyProviderContract.START_TIME + " >= " + startOfCurrentDayInMillis + " AND " +
                        JourneyProviderContract.START_TIME + " < " + endOfCurrentDayInMillis;

                if (initialise){
                    initJourneyList(dailySelection);
                    //setlistviewheigh done in onresume so will be performed after this
                } else {
                    updateAdapterWithNewQuerySelection(dailySelection);
                    setListViewHeightBasedOnChildren(journeyList);
                }
                populateEcoValues(Calendar.DATE);
                //show time not date
                dataAdapter.showDateOnListView(false);
                currentDateFilter = Calendar.DATE;
                break;
        }
    }

    private void addTabListener(TabLayout tabLayout){
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int dateTabSelection = tab.getPosition();
                setAdapterForCurrentDateSelection(dateTabSelection, false);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void updateAdapterWithNewQuerySelection(String selection){
        String sortingOrder = JourneyProviderContract.START_TIME + " DESC";
        dataAdapter.changeCursor(getContentResolver().query(JourneyProviderContract.JOURNEYS_URL,
                journeyColumns, selection, null, sortingOrder));
        dataAdapter.notifyDataSetChanged();
    }

    private long getStartOfCurrentTimePeriodInMillis(int timeField){
        Calendar cal = Calendar.getInstance();
        int year  = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int date  = cal.get(Calendar.DATE);
        cal.clear();
        switch (timeField){
            case Calendar.DATE:
                cal.set(year, month, date);
                break;
            case Calendar.MONTH:
                cal.set(year, month, 1);
                break;
            case Calendar.YEAR:
                cal.set(year, 0, 1);
        }
        long todayMillis = cal.getTimeInMillis();
        return todayMillis;
    }
    private long getEndOfCurrentDayInMillis(){
        Calendar cal = Calendar.getInstance();
        int year  = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int date  = cal.get(Calendar.DATE);
        cal.clear();
        cal.set(year, month, date);
        cal.add(Calendar.DATE, 1);
        long todayMillis = cal.getTimeInMillis();
        return todayMillis;
    }

    /*Taken from: http://www.java2s.com/Code/Android/UI/setListViewHeightBasedOnChildren.htm*/
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        CursorAdapter listAdapter = (CursorAdapter) listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        //for each list item in the list adapter add the height to the total
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        //set the height of the listview to the total height value add the dividers
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    protected void onActivityResult(int requestCode,
                                    int resultCode, Intent data)
    {
        if (requestCode == ADD_NEW_JOURNEY_REQUEST_CODE)
        {
            if (resultCode == RESULT_OK)
            {
                //update the journey list
                setAdapterForCurrentDateSelection(tabLayout.getSelectedTabPosition(), false);
                addTabListener(tabLayout);
            } else if (resultCode == RESULT_CANCELED)
            {
            }
        }
    }

    class JourneyChangeObserver extends ContentObserver
    {

        public JourneyChangeObserver(Handler handler) {
            super(handler);
        }
        @Override
        public void onChange(boolean selfChange) {
            this.onChange(selfChange, null);
        }
        @Override
        public void onChange(boolean selfChange, Uri uri) {
            String sortingOrder = JourneyProviderContract.START_TIME + " DESC";
            dataAdapter.changeCursor(getContentResolver().query(JourneyProviderContract.JOURNEYS_URL,
                    journeyColumns, null, null, sortingOrder));
            dataAdapter.notifyDataSetChanged();
        }
    }
}
