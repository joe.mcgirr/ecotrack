package com.app.labs.mdpcoursework3;

//Very rudimentary ways to calculate CO2 and Cost of fuel
public class FuelCalculations
{
    public static float calculateCostOfJourney(float distance){
        float litresInGallon = 4.54609188f;
        float miles = distance / 1609.344f;
        float milePerGallon = 35f;
        float fuelCost = 124f;
        float gallonsUsed = miles/milePerGallon;
        float cost = (((gallonsUsed * litresInGallon) * fuelCost) / 100);
        float roundedCost = Float.parseFloat(String.format("%.2f", cost));
        return roundedCost;
    }

    public static float calculateCo2CostOfJourney(float distance){
        float averageCo2PerKm = 120; //grams
        float distanceInKm = distance / 1000f;
        float gramsCo2Produced = distanceInKm * averageCo2PerKm;
        float kgCo2Produced = gramsCo2Produced / 1000;
        float roundedCo2 = Float.parseFloat(String.format("%.2f", kgCo2Produced));
        return roundedCo2;
    }
}
