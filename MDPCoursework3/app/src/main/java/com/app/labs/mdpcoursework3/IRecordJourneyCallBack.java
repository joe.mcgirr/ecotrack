package com.app.labs.mdpcoursework3;

public interface IRecordJourneyCallBack
{
    //saving process is finished, boolean describes whether save was successful
    void finishedSavingJourney(boolean success);
    void locationPermissionError();
    void distanceChanged(float distance, double latitude, double longitude);
}
