package com.app.labs.mdpcoursework3.ContentProvider;


import android.net.Uri;

public class JourneyProviderContract
{
    public static final String AUTHORITY =
            "com.app.labs.mdpcoursework3.ContentProvider.JourneyContentProvider";
    public static final Uri JOURNEYS_URL =
            Uri.parse("content://"+AUTHORITY+"/journeys");
    public static final String _ID = "_id";
    public static final String TITLE = "title";
    public static final String MONEY_SAVED = "moneySaved";
    public static final String CO2_SAVED = "co2Saved";
    public static final String MINS_LOST = "minsLost";
    public static final String START_TIME = "startTime";
    public static final String END_TIME = "endTime";
    public static final String TOTAL_JOURNEY_TIME = "totalJourneyTime";
    public static final String DISTANCE_TRAVELLED = "distanceTravelled";
    public static final String START_LAT = "startLat";
    public static final String START_LONG = "startLong";
    public static final String END_LAT = "endLat";
    public static final String END_LONG = "endLong";

    public static final Uri JOURNEY_ROUTE_NODES_URL =
            Uri.parse("content://"+AUTHORITY+"/journeyRouteNodes");
    public static final String JOURNEY_ID = "journey_id";
    public static final String LONGITUDE = "longitude";
    public static final String LATITUDE = "latitude";
    public static final String ALTITUDE = "altitude";
    public static final String TIME = "time";






}

