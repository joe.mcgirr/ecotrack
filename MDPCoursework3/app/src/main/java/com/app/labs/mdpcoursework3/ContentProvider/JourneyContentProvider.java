package com.app.labs.mdpcoursework3.ContentProvider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

public class JourneyContentProvider extends ContentProvider
{
    DBHelper dbHelper;
    private static final UriMatcher uriMatcher;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(JourneyProviderContract.AUTHORITY, "journeys", 1);
        uriMatcher.addURI(JourneyProviderContract.AUTHORITY, "journeys/#", 2);
        uriMatcher.addURI(JourneyProviderContract.AUTHORITY, "journeyRouteNodes", 3);
        uriMatcher.addURI(JourneyProviderContract.AUTHORITY, "journeyRouteNodes/#", 4);
    }

    @Override
    public boolean onCreate()
    {
        Log.d("MDPCW3", "contentprovider oncreate");
        this.dbHelper = new DBHelper(this.getContext(), "journeyDB", null, 1);
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder)
    {

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String tableName = null;
        switch (uriMatcher.match(uri))
        {
            case 1:
                tableName = "journeys";
                break;
            case 3:
                tableName = "journeyRouteNodes";
                break;
        }
        return db.query(tableName, projection, selection, selectionArgs, null, null,
                sortOrder);
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri)
    {
        if (uri.getLastPathSegment() == null)
        {
            return "vnd.android.cursor.dir/MyProvider.data.text";
        } else
        {
            return "vnd.android.cursor.item/MyProvider.data.text";
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues)
    {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String tableName = null;
        switch (uriMatcher.match(uri))
        {
            case 1:
                tableName = "journeys";
                break;
            case 3:
                tableName = "journeyRouteNodes";
                break;
        }
        long id = db.insert(tableName, null, contentValues);
        Uri nu = ContentUris.withAppendedId(uri, id);
        getContext().getContentResolver().notifyChange(nu, null);
        return nu;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String whereClause, @Nullable String[] whereArgs)
    {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String tableName = null;
        switch (uriMatcher.match(uri))
        {
            case 1:
                tableName = "journeys";
                break;
            case 3:
                tableName = "journeyRouteNodes";
                break;
        }
        int id = db.delete(tableName, whereClause, whereArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return id;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String whereClause, @Nullable String[] whereArgs)
    {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String tableName = null;
        switch (uriMatcher.match(uri))
        {
            case 1:
                tableName = "journeys";
                break;
            case 3:
                tableName = "journeyRouteNodes";
                break;
        }
        int id = db.update(tableName, contentValues, whereClause, whereArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return id;
    }
}