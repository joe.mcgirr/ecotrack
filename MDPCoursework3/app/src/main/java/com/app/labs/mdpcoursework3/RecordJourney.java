package com.app.labs.mdpcoursework3;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.labs.mdpcoursework3.BroadcastReceivers.LocationPermissionError;
import com.app.labs.mdpcoursework3.ContentProvider.DBHelper;
import com.app.labs.mdpcoursework3.Services.RecordJourneyService;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

public class RecordJourney extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener
{

    EditText journeyName = null;
    TextInputLayout journeyNameLayout = null;
    TextView title = null;
    TextView promptMessage = null;
    Button startButton = null;
    ProgressBar indeterminateProgressBar = null;
    TextView travelledLabel = null;
    TextView distanceTravelled = null;
    //container for map fragment
    FrameLayout fragmentContainer;
    //google map object
    private GoogleMap mMap;
    //fragment containing map
    private MapFragment mMapFragment;
    //this flag will decide which UI to show, one to start recording, or one showing recording in progress
    boolean isRecording = false;
    RecordJourneyService.RecordJourneyBinder recordJourneyService = null;
    private long journeyId = -1;
    LocationPermissionError broadcastReceiver = null;
    double prevLatPosition = 0;
    double prevLongPosition = 0;
    //needed to return map camera to position on orientation change
    double prevCameraLatPosition = 0;
    double prevCameraLongPosition = 0;
    //stores coords of the drawn path
    ArrayList<LatLng> drawnPath = null;

    static final int JOURNEY_RESULT_ACTIVITY = 1;
    private final String IS_RECORDING = "is_recording";
    private final String JOURNEY_ID = "journey_id";
    private final String PREV_LAT = "previousLat";
    private final String PREV_LONG = "previousLong";
    private static final String PREV_CAM_LAT = "previousCamLat";
    private static final String PREV_CAM_LONG = "previousCamLong";
    private static final String SAVED_PATH = "savedPath";



    IRecordJourneyCallBack journeyCallback = new IRecordJourneyCallBack()
    {
        @Override
        //Called by the service when finished saving journey data to DB
        public void finishedSavingJourney(boolean success)
        {
            if (success)
            {
                openResultsActivity();
            } else
            {
                Toast.makeText(getBaseContext(), R.string.journey_failed_to_save_message, Toast.LENGTH_SHORT).show();
                openResultsActivity();
            }
        }

        //Called by the service if location permission not given
        @Override
        public void locationPermissionError()
        {
            //show error message, stop service, save activity state and return to dashboard
            Toast.makeText(getBaseContext(), R.string.need_location_permission_error, Toast.LENGTH_SHORT).show();
            isRecording = false;
            Intent result = new Intent();
            setResult(Activity.RESULT_CANCELED, result);
            finish();
        }

        @Override
        public void distanceChanged(float distance, double latitude, double longitude)
        {
            String distanceTravelledMiles = DBHelper.formatDistanceTravelled(distance);
            updateMapsCameraLocation(latitude, longitude);
            Log.w("MDPCW3", "Distance changed: " + distanceTravelledMiles);
            distanceTravelled.setText(distanceTravelledMiles + " miles");
        }
    };

    private ServiceConnection journeyServiceConnection = null;

    @Override
    protected void onResume()
    {
        super.onResume();
        if (journeyServiceConnection == null)
        {
            //if user navigates back to start journey from results page, service needs to be reinitialised to record again
            initialiseJourneyServiceConnection();
            initialiseRecordJourneyService();
        }
        if (!isRecording)
        {
            showStartRecordingJourneyUI();
        }
    }

    private void initialiseJourneyServiceConnection()
    {
        journeyServiceConnection = new ServiceConnection()
        {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service)
            {
                Log.w("MDPCW3", "JOURNEY SERVICE CONNECTED TO ACTIVITY");
                recordJourneyService = (RecordJourneyService.RecordJourneyBinder) service;
                recordJourneyService.registerCallback(journeyCallback);
            }

            @Override
            public void onServiceDisconnected(ComponentName name)
            {
                Log.w("MDPCW3", "JOURNEY SERVICE DISCONNECTED FROM ACTIVITY");
                recordJourneyService.unregisterCallback(journeyCallback);
                recordJourneyService = null;
            }
        };
    }

    private void openResultsActivity()
    {
        //start results activity, passing in journey ID
        Intent resultsIntent = new Intent(getBaseContext(), JourneyResults.class);
        Bundle bundle = new Bundle();
        bundle.putLong(JOURNEY_ID, journeyId);
        resultsIntent.putExtras(bundle);
        //make button visible again incase user navigates back
        startActivityForResult(resultsIntent, JOURNEY_RESULT_ACTIVITY);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_journey);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawnPath = new ArrayList<LatLng>();
        //Restores flag that decides whether progress bar on UI is displayed/title changed
        if (savedInstanceState != null)
        {
            isRecording = savedInstanceState.getBoolean(IS_RECORDING);
            journeyId = savedInstanceState.getLong(JOURNEY_ID, -1);
            prevLongPosition = savedInstanceState.getDouble(PREV_LONG, 0.0);
            prevLatPosition = savedInstanceState.getDouble(PREV_LAT, 0.0);
            prevCameraLatPosition =  savedInstanceState.getDouble(PREV_LAT, 0.0);
            prevCameraLongPosition =  savedInstanceState.getDouble(PREV_LONG, 0.0);
            drawnPath = savedInstanceState.getParcelableArrayList(SAVED_PATH);
        }

        assignViewElements();

        initialiseJourneyServiceConnection();
        if (isRecording)
        {
            showJourneyRecordingUI();
            //return camera to orginial postition if loaded from savedInstanceState
            //if journeyId loaded from savedInstanceState
            if (journeyId > 0)
            {
                reInitialiseRecordJourneyService(journeyId);
            }
        } else
        {
            initialiseRecordJourneyService();
        }

        startButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (!isRecording)
                {
                    String name = journeyName.getText().toString();
                    if (name.isEmpty())
                    {
                        Toast.makeText(getBaseContext(), R.string.no_journey_name_message, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    //pass name to service to add to db
                    startRecordingGps(name);
                } else
                {
                    stopRecordingGps();
                    //hide button whilst journey is saving
                    startButton.setVisibility(View.INVISIBLE);
                    //activity will do nothing at this point until callback is called to say journey saved
                }
            }
        });

        registerPermissionErrorReceiver();
    }

    private void initMapFragment()
    {
        mMapFragment = MapFragment.newInstance();
        FragmentTransaction fragmentTransaction =
                getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.map_container, mMapFragment);
        fragmentTransaction.commit();
        mMapFragment.getMapAsync(this);

    }
    //broadcast receiver for permission error custom broadcast
    private void registerPermissionErrorReceiver()
    {
        broadcastReceiver = new LocationPermissionError();
        IntentFilter filter = new IntentFilter("com.app.labs.mdpcoursework3.LocationPermissionError");
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, filter);
    }

    private void stopRecordJourneyService()
    {
        recordJourneyService.unregisterCallback(journeyCallback);
        unbindService(journeyServiceConnection);
        stopService(new Intent(this, RecordJourneyService.class));
    }

    private void stopRecordingGps()
    {
        recordJourneyService.stopRecordingGPS();
        isRecording = false;
        //clear path in case user navs back to this activity from results activity
        drawnPath.clear();
    }

    private void showMap(){
        fragmentContainer.setVisibility(View.VISIBLE);
    }

    private void hideMap(){
        fragmentContainer.setVisibility(View.INVISIBLE);
    }

    private void updateMapsCameraLocation(double newLatitude, double newLongitude){
        //if this is the first routeNode set don't draw a line
        if(isRecording){
            if (prevLatPosition != 0 && prevLongPosition != 0){
                Polyline line = mMap.addPolyline(new PolylineOptions()
                        .add(new LatLng(prevLatPosition, prevLongPosition), new LatLng(newLatitude, newLongitude))
                        .width(5)
                        .color(getResources().getColor(R.color.buttonColor)));
                //add to array of path coords
                drawnPath.addAll(line.getPoints());
            }
        }
        prevLatPosition = newLatitude;
        prevLongPosition = newLongitude;
        //update for when orientation changes
        prevCameraLatPosition = newLatitude;
        prevCameraLongPosition = newLongitude;
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(newLatitude, newLongitude), 18.5f));
    }

    private void startRecordingGps(String name)
    {
        recordJourneyService.setJourneyName(name);
        //start recording gps, return the id of newly created journey
        journeyId = recordJourneyService.startRecordingGPS();
        //< 0 if error occurred when adding new journey to db
        if (journeyId < 0)
        {
            return;
        }
        isRecording = true;
        showJourneyRecordingUI();
    }

    public void assignViewElements()
    {
        journeyName = findViewById(R.id.journey_name_input);
        startButton = findViewById(R.id.start_button);
        title = findViewById(R.id.start_title);
        promptMessage = findViewById(R.id.start_message);
        indeterminateProgressBar = findViewById(R.id.progressBar);
        journeyNameLayout = findViewById(R.id.journey_name_layout);
        distanceTravelled = findViewById(R.id.distance_travelled_counter);
        travelledLabel = findViewById(R.id.travelled_label);
        fragmentContainer = findViewById(R.id.map_container);
    }

    public void initialiseRecordJourneyService()
    {
        this.startService(new Intent(this, RecordJourneyService.class));
        this.bindService(new Intent(this, RecordJourneyService.class), journeyServiceConnection,
                Context.BIND_AUTO_CREATE);
    }

    //Called when activity destroyed while recording on screen orientation
    public void reInitialiseRecordJourneyService(long journeyId)
    {
        Intent intent = new Intent(this, RecordJourneyService.class);
        intent.putExtra("JOURNEY_ID", journeyId);
        this.startService(intent);
        this.bindService(new Intent(this, RecordJourneyService.class), journeyServiceConnection,
                Context.BIND_AUTO_CREATE);
    }

    //display progress wheel, hide the rest
    public void showJourneyRecordingUI()
    {
        initMapFragment();
        showMap();
        travelledLabel.setVisibility(View.VISIBLE);
        distanceTravelled.setVisibility(View.VISIBLE);
        journeyNameLayout.setVisibility(View.INVISIBLE);
        title.setVisibility(View.INVISIBLE);
        promptMessage.setVisibility(View.INVISIBLE);
        startButton.setText(getResources().getString(R.string.journey_recording_button));
    }

    //displays start recording button, title field etc
    public void showStartRecordingJourneyUI()
    {
        hideMap();
        journeyNameLayout.setVisibility(View.VISIBLE);
        title.setVisibility(View.VISIBLE);
        promptMessage.setVisibility(View.VISIBLE);
        startButton.setVisibility(View.VISIBLE);
        startButton.setText(getResources().getString(R.string.start_recording_button_text));
        travelledLabel.setVisibility(View.INVISIBLE);
        distanceTravelled.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState)
    {
        savedInstanceState.putBoolean(IS_RECORDING, isRecording);
        savedInstanceState.putLong(JOURNEY_ID, journeyId);
        savedInstanceState.putDouble(PREV_LAT, prevLatPosition);
        savedInstanceState.putDouble(PREV_LONG, prevLongPosition);
        savedInstanceState.putDouble(PREV_CAM_LAT, prevCameraLatPosition);
        savedInstanceState.putDouble(PREV_CAM_LONG, prevCameraLongPosition);
        savedInstanceState.putParcelableArrayList(SAVED_PATH, drawnPath);

        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onStop()
    {
        super.onStop();
    }

    protected void onDestroy()
    {
        if (journeyServiceConnection != null)
        {
            //stop the service onDestroy, will restart if necessary with the journeyId saved in onSavedInstanceState
            stopRecordJourneyService();
            recordJourneyService = null;
            journeyServiceConnection = null;
        }
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }

    @Override
    public void onBackPressed()
    {
        recordJourneyService.discardCurrentJourneyFromDb();
        //set recording to false so correct UI shows when dashboard navs back here
        isRecording = false;
        //finish activity so onDestroy is called and journey service is stopped
        Intent result = new Intent();
        setResult(Activity.RESULT_CANCELED, result);
        finish();
    }

    protected void onActivityResult(int requestCode,
                                    int resultCode, Intent data)
    {
        if (requestCode == JOURNEY_RESULT_ACTIVITY)
        {
            //Ok button clicked on results page
            if (resultCode == RESULT_OK)
            {
                //returns user to dashboard
                setResult(Activity.RESULT_OK, new Intent());
                finish();
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setMinZoomPreference(5.0f);
        mMap.setPadding(0, 0, 0, 30);
        //permission will already be granted
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);

        //update map with current location
        if(recordJourneyService != null){
            Location currentLocation = recordJourneyService.getCurrentDeviceLocation();
            updateMapsCameraLocation(currentLocation.getLatitude(), currentLocation.getLongitude());
        }

        //if previous camera position saved (on orientation) return camera to position and restore path
        if(prevCameraLatPosition != 0 && prevLongPosition != 0){
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(prevCameraLatPosition, prevCameraLongPosition), 18.5f));
            restorePath();
        }
    }

    private void restorePath(){
        Polyline line = mMap.addPolyline(new PolylineOptions()
                .addAll(drawnPath)
                .width(5)
                .color(getResources().getColor(R.color.buttonColor)));
        drawnPath.addAll(line.getPoints());
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        Log.w("MDPCW3", "my location clicked");
    }

    @Override
    public boolean onMyLocationButtonClick() {
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        Log.w("MDPCW3", "my location clicked");

        return false;
    }
}
