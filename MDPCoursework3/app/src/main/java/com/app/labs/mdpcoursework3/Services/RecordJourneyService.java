package com.app.labs.mdpcoursework3.Services;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Criteria;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteCallbackList;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.labs.mdpcoursework3.ContentProvider.JourneyProviderContract;
import com.app.labs.mdpcoursework3.FuelCalculations;
import com.app.labs.mdpcoursework3.IRecordJourneyCallBack;
import com.app.labs.mdpcoursework3.R;
import com.app.labs.mdpcoursework3.RecordJourney;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class RecordJourneyService extends Service
{
    private static final int MY_PERMISSIONS_REQUEST_PROCESS_CALLS = 1;
    private final int NOTIFICATION_ID = 001; // a unique int for each notification
    private final String CHANNEL_ID = "100";
    LocationManager locationManager = null;
    String title = null;
    long journeyId = 0;
    long startTime = 0;
    long endTime = 0;
    MyLocationListener locationListener = null;
    //please don't rob me...
    String mapsApiKey;

    RemoteCallbackList<RecordJourneyBinder> remoteCallbackList = new RemoteCallbackList<RecordJourneyBinder>();

    public RecordJourneyService()
    {
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        Log.w("MDPCW3", "RECORD JOURNEY BIND");
        return new RecordJourneyBinder();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        //if journey id passed to service, resume recording for that journey rather than making a new one
        if (intent.getExtras() != null)
        {
            resumeRecordingGps(intent.getExtras().getLong("JOURNEY_ID"));
        }
        return super.onStartCommand(intent, flags, startId);
    }


    @Override
    public boolean onUnbind(Intent intent)
    {
        Log.w("MDPCW3", "RECORD JOURNEY UNBIND");
        return super.onUnbind(intent);
    }

    @Override
    public void onCreate()
    {
        Log.w("MDPCW3", "JOURNEY SERVICE CREATED");
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mapsApiKey = getResources().getString(R.string.google_maps_key);
        super.onCreate();
    }

    /*Methods that binder calls*/
    public void stopRecordingGPS()
    {
        Log.w("MDPCW3", "SERVICE STOP RECORDING GPS");
        //stop recording distance
        if (locationListener != null)
        {
            locationManager.removeUpdates(locationListener);
        }
        this.endTime = System.currentTimeMillis();
        updateAndSaveJourneyRecording();
    }

    private Location getDeviceCurrentLocation()
    {
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers)
        {
            //permission should already be granted
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {
               /* ActivityCompat.requestPermissions(getActi(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST_PROCESS_CALLS
                );*/
                return null;
            }
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }


    public long startRecordingGPS()
    {
        Log.w("MDPCW3", "SERVICE START RECORDING GPS");
        //successful if correct permissions granted
        if (initialiseLocationListener())
        {
            startTime = System.currentTimeMillis();
            showNotificationAndRunInForeground();
            //add new journey to db and return its ID
            return addNewJourneyWithTitleToDb();
        }
        //shows startjouneyActivity permisssions not granted
        return -1;
    }

    private void resumeRecordingGps(long journeyId)
    {
        this.journeyId = journeyId;
        if (initialiseLocationListener())
        {
            startTime = getJourneyStartTime();
            showNotificationAndRunInForeground();
        }
    }

    //stops distances returning null
    private Location handleNullLocation(Location location)
    {
        Location nullHandledLocation = new Location("");//provider name is unnecessary
        if (location == null)
        {
            nullHandledLocation.setLatitude(0d);
            nullHandledLocation.setLongitude(0d);
            return nullHandledLocation;
        } else
        {
            return location;
        }

    }

    private void updateAndSaveJourneyRecording()
    {
        final Location startLocation = handleNullLocation(getFirstJourneyRouteNodeInSequence(JourneyProviderContract.TIME + " ASC"));
        final Location endLocation = handleNullLocation(getFirstJourneyRouteNodeInSequence(JourneyProviderContract.TIME + " DESC"));
        final long journeyDuration = endTime - startTime;

        /*creates HTTP Request*/
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" + startLocation.getLatitude() + "," + startLocation.getLongitude() +
                "&destinations=" + endLocation.getLatitude() + "," + endLocation.getLongitude() + "&mode=driving&key=" + mapsApiKey;

        // Request a JSON response from the provided URL.
        JsonObjectRequest mapsApiRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        long minsLost = 0;
                        float moneySaved = 0;
                        float co2Saved = 0;

                        int distanceInCar = getDistanceValueFromApiResponse(response);
                        int durationInCar = getDurationValueFromApiResponse(response);
                        long currentJourneyDurationInMins = journeyDuration / 60000L;

                        minsLost = currentJourneyDurationInMins - durationInCar;
                        moneySaved = FuelCalculations.calculateCostOfJourney((float) distanceInCar);
                        co2Saved = FuelCalculations.calculateCo2CostOfJourney((float) distanceInCar);

                        int result = updateJourneyRecording(moneySaved, co2Saved, minsLost, startTime, endTime, journeyDuration,
                                startLocation, endLocation);

                        if (result > 0)
                        {
                            Log.w("MDPCW3", "updated journey successfully");
                            removeRouteDataFromDb();
                            alertClientsJourneySaved(true);
                        } else
                        {
                            Log.w("MDPCW3", "Failed to update journey");
                            alertClientsJourneySaved(false);
                        }

                        //kill service when done
                        stopForeground(true);
                        stopSelf();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.w("MDPCW3", "Failed to get response from API " + error);
                    }
                });

        // Add the request to the RequestQueue.
        queue.add(mapsApiRequest);
    }

    //Update journey in db with given values
    private int updateJourneyRecording(float moneySaved, float co2Saved, long minsLost, long startTime, long endTime,
                                       long journeyDuration, Location startLocation, Location endLocation)
    {

        ContentValues updateValues = new ContentValues();
        updateValues.put(JourneyProviderContract.MONEY_SAVED, moneySaved);
        updateValues.put(JourneyProviderContract.CO2_SAVED, co2Saved);
        updateValues.put(JourneyProviderContract.MINS_LOST, minsLost);
        updateValues.put(JourneyProviderContract.START_TIME, startTime);
        updateValues.put(JourneyProviderContract.END_TIME, endTime);
        updateValues.put(JourneyProviderContract.TOTAL_JOURNEY_TIME, journeyDuration);
        updateValues.put(JourneyProviderContract.START_LAT, startLocation.getLatitude());
        updateValues.put(JourneyProviderContract.START_LONG, startLocation.getLongitude());
        updateValues.put(JourneyProviderContract.END_LAT, endLocation.getLatitude());
        updateValues.put(JourneyProviderContract.END_LONG, endLocation.getLongitude());

        int result = getContentResolver().update(
                JourneyProviderContract.JOURNEYS_URL,
                updateValues,
                "_id=" + journeyId,
                null
        );

        return result;
    }

    private int getDistanceValueFromApiResponse(JSONObject response)
    {
        try
        {
            JSONArray rows = response.getJSONArray("rows");
            JSONArray elements = rows.getJSONObject(0).getJSONArray("elements");
            JSONObject distance = elements.getJSONObject(0).getJSONObject("distance");
            int distanceValue = distance.getInt("value");
            return distanceValue;

        } catch (JSONException e)
        {
            e.printStackTrace();
            return 0;
        }
    }

    private int getDurationValueFromApiResponse(JSONObject response)
    {
        try
        {
            JSONArray rows = response.getJSONArray("rows");
            JSONArray elements = rows.getJSONObject(0).getJSONArray("elements");
            JSONObject duration = elements.getJSONObject(0).getJSONObject("duration");
            int durationValue = duration.getInt("value") / 60;
            return durationValue;

        } catch (JSONException e)
        {
            e.printStackTrace();
            return 0;
        }
    }

    //no longer need route data if all info has been saved to journey correctly
    private int removeRouteDataFromDb()
    {
        int deleted = getContentResolver().delete(
                JourneyProviderContract.JOURNEY_ROUTE_NODES_URL, "journey_id=" + journeyId, null);
        return deleted;
    }

    private int removeCurrentJourneyFromDb()
    {
        int deleted = getContentResolver().delete(
                JourneyProviderContract.JOURNEYS_URL, "_id=" + journeyId, null);
        if (deleted < 1)
        {
            Log.w("MDPCW3", "Error deleting current journey from db");
        }
        return deleted;
    }

    //argument tells client if saved successfully, or error
    private void alertClientsJourneySaved(boolean success)
    {
        final int n = remoteCallbackList.beginBroadcast();
        for (int i = 0; i < n; i++)
        {
            remoteCallbackList.getBroadcastItem(i).callback.finishedSavingJourney(success);
        }
        remoteCallbackList.finishBroadcast();
    }

    //send permission broadcast to all clients
    private void broadCastPermissionError()
    {
        //when received dialogue opens to allow user to enable/deny location settings
        sendLocationPermissionRequiredBroadcast();

        //returns to dashboard and displays error toast - gives users nice indication of why app not working
        final int n = remoteCallbackList.beginBroadcast();
        for (int i = 0; i < n; i++)
        {
            remoteCallbackList.getBroadcastItem(i).callback.locationPermissionError();
        }
        remoteCallbackList.finishBroadcast();
    }

    private void notifyClientsOfDistanceChange(float distance, double latitude, double longitude)
    {
        final int n = remoteCallbackList.beginBroadcast();
        for (int i = 0; i < n; i++)
        {
            remoteCallbackList.getBroadcastItem(i).callback.distanceChanged(distance, latitude, longitude);
        }
        remoteCallbackList.finishBroadcast();
    }

    private void sendLocationPermissionRequiredBroadcast(){
        Intent intent = new Intent();
        intent.setAction("com.app.labs.mdpcoursework3.LocationPermissionError");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    //retrieve start time for existing db if service is stopped and restarted without finishing
    private long getJourneyStartTime(){
        Cursor cursor = getContentResolver().query(
                JourneyProviderContract.JOURNEYS_URL,
                new String[]{JourneyProviderContract.START_TIME},     // The columns to return for each row
                "_id=" + journeyId,
                null,
                null);

        if(cursor.moveToFirst()){
            startTime = cursor.getLong(cursor.getColumnIndexOrThrow(JourneyProviderContract.START_TIME));
            return startTime;
        } else {
            Log.w("MDPCW3", "Failed to load start time from DB");
            return  0;
        }
    }

    //get the first journeyRouteNode entity from an ordered query
    private Location getFirstJourneyRouteNodeInSequence(String sortingOrder){
        Cursor cursor = getContentResolver().query(
                JourneyProviderContract.JOURNEY_ROUTE_NODES_URL,
                new String[]{JourneyProviderContract.LONGITUDE, JourneyProviderContract.LATITUDE, JourneyProviderContract.ALTITUDE, JourneyProviderContract.TIME},                       // The columns to return for each row
                "journey_id=" + journeyId,
                null,
                sortingOrder);

        //convert to location objects
        if(cursor.moveToFirst()){
            Location location = new Location("");
            location.setLatitude(cursor.getDouble(cursor.getColumnIndexOrThrow(JourneyProviderContract.LATITUDE)));
            location.setLongitude(cursor.getDouble(cursor.getColumnIndexOrThrow(JourneyProviderContract.LONGITUDE)));
            return location;
        }else {
            Log.w("MDPCW3", "Query failed to load journey node");
            return null;
        }
    }

    //add initial journey entity
    private long addNewJourneyWithTitleToDb(){
        ContentValues newValues = new ContentValues();
        newValues.put(JourneyProviderContract.TITLE, title);
        newValues.put(JourneyProviderContract.START_TIME, startTime);
        newValues.put(JourneyProviderContract.MONEY_SAVED, 0);
        newValues.put(JourneyProviderContract.CO2_SAVED, 0);

        Uri result = getContentResolver().insert(JourneyProviderContract.JOURNEYS_URL, newValues);
        journeyId = Long.parseLong(result.getLastPathSegment());

        return journeyId;
    }

    private void setName(String name)
    {
        this.title = name;
    }

    public void showNotificationAndRunInForeground(){
        /*Create notification and make service run in the foreground*/
        buildNotificationForAPI26Plus();
        createPendingIntentForNotification();
    }

    public void buildNotificationForAPI26Plus(){
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Journey Recording";
            String description = "You're currently recording your journey, click here to stop recording";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            notificationManager.createNotificationChannel(channel);
        }
    }

    public void createPendingIntentForNotification(){

        Intent intent = new Intent(RecordJourneyService.this, RecordJourney.class);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        Intent actionIntent = new Intent(RecordJourneyService.this, RecordJourneyService.class);
        PendingIntent pendingActionIntent = PendingIntent.getService(this, 0, actionIntent, 0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle("Tracking journey")
                .setContentText("stop tracking journey?")
                .setContentIntent(pendingIntent)
                .addAction(R.drawable.ic_launcher_foreground, "Message Service", pendingActionIntent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        startForeground(NOTIFICATION_ID, mBuilder.build());
    }

    //returns false if correct permissions not granted
    public boolean initialiseLocationListener(){
        locationListener = new MyLocationListener();
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    5, // minimum time interval between updates
                    5, // minimum distance between updates, in metres
                    locationListener);
            return true;
        } catch(SecurityException e) {

            broadCastPermissionError();
            return false;
        }
    }

    @Override
    public void onDestroy()
    {
        //cancel notification
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);
        //stops listening to location changes
        if (locationListener != null){
            locationManager.removeUpdates(locationListener);
        }

        Log.w("MDPCW3", "JOURNEY SERVICE DESTROYED");
        super.onDestroy();
    }

    public class RecordJourneyBinder extends Binder implements IInterface
    {
        IRecordJourneyCallBack callback;
        @Override
        public IBinder asBinder()
        {
            return this;
        }

        public void registerCallback(IRecordJourneyCallBack callback) {
            this.callback = callback;
            remoteCallbackList.register(RecordJourneyBinder.this);
        }

        public void unregisterCallback(IRecordJourneyCallBack callback) {
            remoteCallbackList.unregister(RecordJourneyBinder.this);
        }

        public long startRecordingGPS(){
            return RecordJourneyService.this.startRecordingGPS();
        }

        public void stopRecordingGPS(){
            RecordJourneyService.this.stopRecordingGPS();
        }
        public void setJourneyName(String name){
            RecordJourneyService.this.setName(name);
        }
        public void discardCurrentJourneyFromDb(){RecordJourneyService.this.removeCurrentJourneyFromDb();}
        public Location getCurrentDeviceLocation(){return RecordJourneyService.this.getDeviceCurrentLocation();}
    }



    public class MyLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            updateJourneyDistanceTravelled(location);
            addNewLocationAsJourneyRouteNode(location);
        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            // information about the signal, i.e. number of satellites
            Log.w("MDPCW3", "onStatusChanged: " + provider + " " + status);
        }
        @Override
        public void onProviderEnabled(String provider) {
            // the user enabled (for example) the GPS
           // Log.w("MDPCW3", "onProviderEnabled: " + provider);
            //if timer paused run timer
        }
        @Override
        public void onProviderDisabled(String provider) {
            // the user disabled (for example) the GPS
            Log.d("g53mdp", "onProviderDisabled: " + provider);
            //if timer runnning, pause timer
        }

        /*HELPER METHODS*/
        private void updateJourneyDistanceTravelled(Location newLocation){
            ContentValues updatedValues = new ContentValues();
            Location latestRouteNode = getFirstJourneyRouteNodeInSequence(JourneyProviderContract.TIME + " DESC");
            float journeyDistanceSoFar = getDistanceTravelledForJourney();

            if(latestRouteNode != null){
                float distanceTravelled = latestRouteNode.distanceTo(newLocation);
                float newDistanceTravelled = journeyDistanceSoFar + distanceTravelled;
                //add new distance to distance travelled so far
                notifyClientsOfDistanceChange(newDistanceTravelled, newLocation.getLatitude(), newLocation.getLongitude());
                updatedValues.put(JourneyProviderContract.DISTANCE_TRAVELLED, newDistanceTravelled);
                int result = getContentResolver().update(JourneyProviderContract.JOURNEYS_URL, updatedValues, "_id="+ journeyId,
                        null);
                if(result > 0){
                    Log.w("MDPCW3", "JOURNEY DISTANCE UPDATED: New distance = " + (journeyDistanceSoFar + distanceTravelled));
                } else {
                        Log.w("MDPCW3", "ERROR UPDATING DISTANCE FOR NEW NODE: T Distance = " + distanceTravelled);
                }
            }
        }

        public float getDistanceTravelledForJourney(){
            Cursor journeyCursor = getContentResolver().query(JourneyProviderContract.JOURNEYS_URL, new String[]{JourneyProviderContract.DISTANCE_TRAVELLED},
                    "_id=" + journeyId,null, null);
            if(journeyCursor.moveToFirst()){
                return journeyCursor.getFloat(journeyCursor.getColumnIndexOrThrow(JourneyProviderContract.DISTANCE_TRAVELLED));
            }
            Log.w("MDPCW3", "DISTANCE TRAVELLED FAILED TO RESOLVE FOR JOURNEY id: " + journeyId);
            return 0;
        }

        private void addNewLocationAsJourneyRouteNode(Location location){
            //stores location data in journeyRouteNode table for journey
            ContentValues newValues = new ContentValues();
            newValues.put(JourneyProviderContract.JOURNEY_ID, journeyId);
            newValues.put(JourneyProviderContract.LONGITUDE, location.getLongitude());
            newValues.put(JourneyProviderContract.LATITUDE, location.getLatitude());
            newValues.put(JourneyProviderContract.ALTITUDE, location.getAltitude());
            newValues.put(JourneyProviderContract.TIME, System.currentTimeMillis());
            getContentResolver().insert(JourneyProviderContract.JOURNEY_ROUTE_NODES_URL, newValues);
        }
    }
}
